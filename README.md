# Processamento da Informação - 2018.Q1

Códigos-fonte utilizados ou desenvolvidos em aula. Os códigos estão organizados em uma pasta para cada aula. Cada uma dessas pastas é um projeto Netbeans. Os programas estão na pasta ```src``` de cada projeto, um arquivo Java por programa.

Para clonar esse repositório, você deve executar:

```
git clone https://diogo_martins@bitbucket.org/diogo_martins/pi-2018q1.git
```

Para atualizar o seu repositório local com os novos conteúdos publicados ao longo do quadrimestre, execute na raiz do repositório:

```
git remote update
git pull
```

Os comandos de atualização só funcionarão satisfatoriamente se você não tiver editado arquivos no seu repositório local (pois você não tem autorização para realizar commits no repositório remoto). Nesse caso, basta remover o diretório do repositório local e clonar novamente.
