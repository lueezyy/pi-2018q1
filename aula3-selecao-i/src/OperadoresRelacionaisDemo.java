/**
 * Demonstra os operadores relacionais
 */
public class OperadoresRelacionaisDemo {
    public static void main(String[] args) {
        System.out.println(1 < 2);
        System.out.println(3 < 4);
        System.out.println(1 <= 1);
        System.out.println(2 >= 2);
        System.out.println('a' == 'a');
        System.out.println('a' != 'b');
        System.out.println('a' < 'b');
    }
}
