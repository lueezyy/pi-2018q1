
import java.util.Random;

/**
 *
 * Demonstra a geracao de inteiros aleatorios
 */
public class InteirosAleatorios {
    public static void main(String[] args) {
        // instancia o gerador de numeros aleatorios
        Random random = new Random();
        
        // gera inteiros com diferentes limiares superiores (exclusivos)
        System.out.println(random.nextInt());
        System.out.println(random.nextInt(10));
        System.out.println(random.nextInt(100));
    }
}
