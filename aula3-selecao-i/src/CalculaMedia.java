
import java.util.Scanner;

/**
 * Calcula media de tres notas e diz se implica aprovacao
 */
public class CalculaMedia {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        float n1, n2, n3;
        float media;
        
        System.out.print("Digite a primeira nota: ");
        n1 = input.nextFloat();
        System.out.print("Digite a segunda nota: ");
        n2 = input.nextFloat();
        System.out.print("Digite a terceira nota: ");
        n3 = input.nextFloat();
        
        media = (n1 + n2 + n3) / 3f;
        System.out.println(String.format("A media eh: %f", media));
        if (media >= 6.0)
            System.out.println("O aluno estah aprovado");
    }
}
