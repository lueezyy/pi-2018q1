/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author diogo
 */
public class WrappeClassDemo {
    public static void main(String[] args) {        
        byte    aByte   = Byte.parseByte("123");
        char    aChar   = "a".charAt(0);
        boolean aBool   = Boolean.parseBoolean("true");
        short   aShort  = Short.parseShort("11");
        int     aInt    = Integer.parseInt("10");
        long    aLong   = Long.parseLong("4896");
        float   aFloat  = Float.parseFloat("1.0");
        double  aDouble = Double.parseDouble("2.0");
    }
    
}
