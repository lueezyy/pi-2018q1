/**
 * Demonstra a captura de argumentos de programa.
 * Para testar, eh preciso executar via terminal (ou definir os argumentos
 * do programa na configuracao do Netbeans)
 */
public class SaluteMe {
    public static void main(String[] args) {
        String nome = args[0];
        String sobrenome = args[1];
        
        System.out.printf("Ola %s %s. Como vai?\n", nome, sobrenome);
    }
}
