/**
 *
 * Demonstra como validar a quantidade de argumentos passados para o programa
 */
public class SaluteMeValida {
    
    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Uso: SaluteMeValida <nome> <sobrenome>");
            
            System.exit(1);
        }
        String nome = args[0];
        String sobrenome = args[1];
        
        System.out.printf("Ola %s %s. Como vai?\n", nome, sobrenome);
    }
    
}
