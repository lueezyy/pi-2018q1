
import java.util.Scanner;

/**
 *
 * Determina as raizes de um polinomio quadratico a partir dos coeficientes
 * 
 */
public class RaizesPolinomioQuadratico {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a, b, c, d;
        double delta, y;
        
        System.out.println("Considere y=ax^2+bx+c");
        System.out.print("a=");
        a = scanner.nextDouble();
        if (a == 0) {
            // nao eh funcao quadratica
            System.err.println("Coeficiente invalido");
            System.exit(1);
        }
        System.out.print("b=");
        b = scanner.nextDouble();
        System.out.print("c=");
        c = scanner.nextDouble();
        
        delta = b * b - 4 * a * c;
        d = Math.sqrt(delta);
        
        if (a > 0) {
            System.out.println("Concavidade para cima");
        } else {
            System.out.println("Concavidade para baixo");
        }
        
        if (delta < 0) { // nenhuma raiz real
            System.out.println("Sem raizes reais");
        } else { // ao menos duas raizes reais
            System.out.printf("y0=%f\n", (-b + d) / (2 * a));
        }
        if (delta > 0) { // duas raizes reais
            System.out.printf("y1=%f\n", (-b - d) / (2 * a));
        }
    }
}
