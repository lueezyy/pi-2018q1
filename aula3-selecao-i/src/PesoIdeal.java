
import java.util.Scanner;

/**
 *
 * Determina o peso ideal de acordo com o sexo e a altura
 * Utiliza apenas selecao simples
 */
public class PesoIdeal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char sexo;
        float altura;
        float pesoIdeal = 0;
        
        System.out.print("Sexo (M/F): ");
        sexo = scanner.next().charAt(0);
        System.out.print("Altura (metros): ");
        altura = scanner.nextFloat();
        
        if (sexo == 'M') {
            pesoIdeal = 72.7f * altura - 58f;
        } 
        if (sexo == 'F') {
            pesoIdeal = 62.1f * altura - 44.7f;
        } 
        System.out.printf("Peso ideal: %f\n", pesoIdeal);
    }
}
