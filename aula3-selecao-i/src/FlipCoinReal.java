
import java.util.Random;

/**
 *
 * Simula o lancamento de uma moeda e informa o resultado
 */
public class FlipCoinReal {
    public static void main(String[] args) {
        if (Math.random() > 0.5) {
            System.out.println("Cara");
        } else {
            System.out.println("Coroa");
        }
    }
}
