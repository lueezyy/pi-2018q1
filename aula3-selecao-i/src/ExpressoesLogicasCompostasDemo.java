/**
 * Demonstra a avaliacao de expressoes logicas compostas
 */
public class ExpressoesLogicasCompostasDemo {
    public static void main(String[] args) {
        int x = 10;
        char y = 'a';
        int z = -1;
        boolean d;
        
        d = x > 5 && y == 'a';
        System.out.println(d);
        d = x > 5 || y == 'b';
        System.out.println(d);
        d = x > 5 && y == 'b';
        System.out.println(d);
        d = x > 5 && y == 'a' || z > 0;
        System.out.println(d);
        d = !(x > 5 && y == 'b') && z < 0;
        System.out.println(d);
        d = x > y;
        System.out.println(d); 
    }
}
