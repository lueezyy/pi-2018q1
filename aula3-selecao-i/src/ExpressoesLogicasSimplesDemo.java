/**
 * Demonstra a avaliacao de expressoes logicas simples
 */
public class ExpressoesLogicasSimplesDemo {
    public static void main(String[] args) {
        boolean a = true;
        boolean b = false;
        boolean c = true;
        boolean d;
        
        d = a && b && c;
        System.out.println(d);
        d = a && b || c;
        System.out.println(d);
        d = a || b || c;
        System.out.println(d);
        d = a && b && !c;
        System.out.println(d);
    }
}
