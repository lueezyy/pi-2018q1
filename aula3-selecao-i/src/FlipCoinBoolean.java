
import java.util.Random;

/**
 *
 * Demonstra o uso de booleanos aleatorios para simular lancamento de moeda
 * Complementarmente, demonstra um possivel uso do operador ternario
 */
public class FlipCoinBoolean {
    public static void main(String[] args) {
        Random random = new Random();
        
        System.out.println((random.nextBoolean()) ? "Cara" : "Coroa");
    }
}
