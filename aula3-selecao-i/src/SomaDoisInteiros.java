/**
 * Soma dois numeros inteiros passados como argumento para o programa
 * Demonstra como converter tipos de dados
 */
public class SomaDoisInteiros {
    public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Uso: SomaDoisInteiros <a> <b>");
            System.exit(1);
        }
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        
        System.out.printf("%d + %d = %d\n", a, b, a + b);
    }
}
