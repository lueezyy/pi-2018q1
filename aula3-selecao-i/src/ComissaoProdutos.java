
import java.util.Scanner;

/** 
 * Calcula a comissao de um produto com base na quantidade vendida
 * Utiliza selecoes simples, pode ser melhorado com selecao encadeada (veremos
 * depois)
 */
public class ComissaoProdutos {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int qtdeProd;
        float comissao = 0;
        
        System.out.print("Qtde. produtos: ");
        qtdeProd = scanner.nextInt();
        
        if (qtdeProd > 0 && qtdeProd < 250) {
            comissao = qtdeProd;
        }
        if (qtdeProd >= 250 && qtdeProd < 500) {
            comissao = qtdeProd * 1.5f;
        }
        if (qtdeProd >= 500) {
            comissao = qtdeProd * 2.0f;
        }
        
        System.out.printf("Comissao: R$%.2f\n", comissao);
    }
}
