
import java.util.Scanner;

/**
 * Leh dois inteiros x e y e garante que o menor estah em x e o maior em y
 */
public class MaiorMenor {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int x, y;
        
        System.out.print("x = ");
        x = scanner.nextInt();
        System.out.print("y = ");
        y = scanner.nextInt();
        
        if (x > y) {
            int t = x;
            x = y;
            y = t;
        }
        
        System.out.printf("x = %d\n", x);
        System.out.printf("y = %d\n", y);
    }
}
