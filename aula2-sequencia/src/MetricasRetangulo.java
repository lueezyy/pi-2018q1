import java.util.Scanner;

/**
 * Calcula o perimetro, area e diagonal de um retangulo
 */
public class MetricasRetangulo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int b, a;
        
        System.out.print("Digite a base: ");
        b = scanner.nextInt();
        System.out.print("Digite a altura: ");
        a = scanner.nextInt();
        
        System.out.printf("Perimetro: %d\n", 2*a+2*b);
        System.out.printf("Area: %d\n", b*a);
        System.out.printf("Diagonal: %f\n", Math.sqrt(a*a + b*b));
    }
}
