
import java.util.Scanner;

/**
 *
 * Efetua a soma de tres numeros inteiros
 */
public class SomaTresNumeros {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a, b, c;
        
        System.out.print("Digite o 1o inteiro: ");
        a = scanner.nextInt();
        
        System.out.print("Digite o 2o inteiro: ");
        b = scanner.nextInt();
        
        System.out.print("Digite o 3o inteiro: ");
        c = scanner.nextInt();
        
        System.out.printf("%d+%d+%d=%d\n", a, b, c, a+b+c);
    }
}
