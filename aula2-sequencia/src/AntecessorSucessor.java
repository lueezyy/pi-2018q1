

import java.util.Scanner;

/**
 * Le um numero inteiro e imprime seu sucessor e seu antecessor
 */
public class AntecessorSucessor {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a;
        
        System.out.print("Digite um nro. inteiro: ");
        a = scanner.nextInt();
        
        System.out.printf("Sucessor: %d\n", a - 1);
        System.out.printf("Antecessor: %d\n", a + 1);
        
    }
}
