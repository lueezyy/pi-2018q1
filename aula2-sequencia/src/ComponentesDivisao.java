
import java.util.Scanner;

/**
 *
 * Demonstra como efetuar divisoes inteiras
 */
public class ComponentesDivisao {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        int a, b;
        
        System.out.print("Digite o primeiro inteiro: ");
        a = scanner.nextInt();
        System.out.print("Digite o segundo inteiro: ");
        b = scanner.nextInt();
        
        System.out.printf("Dividendo: %d\n", a);
        System.out.printf("Divisor: %d\n", b);
        System.out.printf("Quociente: %d\n", a/b);
        System.out.printf("Resto: %d\n", a % b);
        System.out.println();
        System.out.printf("Dividendo: %d\n", b);
        System.out.printf("Divisor: %d\n", a);
        System.out.printf("Quociente: %d\n", b/a);
        System.out.printf("Resto: %d\n", b % a);
    }
    
}
