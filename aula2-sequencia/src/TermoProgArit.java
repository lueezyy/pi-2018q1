
import java.util.Scanner;

/**
 * Calcula o 20o. termo de uma PA 
 */
public class TermoProgArit {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int r, a0;
        
        System.out.print("Razao: ");
        r = scanner.nextInt();
        System.out.printf("1o. termo: ");
        a0 = scanner.nextInt();
        
        System.out.printf("20o. termo: %d\n", a0 + (20-1) * r);
    }
}
