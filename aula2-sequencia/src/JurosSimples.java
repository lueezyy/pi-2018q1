import java.util.Scanner;

/**
 *
 * @author diogo
 */
public class JurosSimples {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float p;
        float i;
        float n;
        
        System.out.print("Digite o capital: ");
        p = scanner.nextFloat();
        System.out.print("Digite o taxa de juros: ");
        i = scanner.nextFloat();
        System.out.print("Digite o periodo: ");
        n = scanner.nextFloat();
        
        System.out.printf("Divida: %f\n", p + p * i *n);
    }
}
