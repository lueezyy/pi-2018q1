/**
 * Exemplo trivial que demonstra a permuta de valores entre duas variaveis
 * @author diogo
 */
public class TrocaVariaveis {
    
    public static void main(String[] args) {
        // declaracao
        int a, b, t;
        
        // valores iniciais
        a = 1;
        b = 2;
        System.out.printf("Pre-permuta: a=%d, b=%d\n", a, b);
        
        // permuta
        t = a;
        a = b;
        b = t;
        System.out.printf("Pos-permuta: a=%d, b=%d\n", a, b);
    }    
}
