import java.util.Scanner;

/**
 *
 * Converte Fahrenheit para Celsius
 */
public class Fahrenheit2Celsius {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float f;
        
        System.out.print("Temperatura (F): ");
        f = scanner.nextFloat();
        System.out.printf("%f F = %f C\n", f, (f - 32) / 1.8);
    }
}
