
import java.util.Scanner;

/**
 * Calcula o 20o. termo de uma PG 
 */
public class TermoProgGeom {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int r, a0;
        
        System.out.print("Razao: ");
        r = scanner.nextInt();
        System.out.printf("1o. termo: ");
        a0 = scanner.nextInt();
        
        System.out.printf("20o. termo: %d\n", a0 * (int )Math.pow(r, 20-1));
    }
}
