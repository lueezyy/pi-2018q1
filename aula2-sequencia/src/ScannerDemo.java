
import java.util.Scanner;

/**
 *
 * Demonstra como utilizar a classe Scanner para realizar entrada interativa
 * de dados
 * 
 * Complementarmente, demonstra como formatar diferentes tipos de dados
 * para saida com printf
 */
public class ScannerDemo {
   
    public static void main(String[] args) {
        // declaracao (note que precisamos tambem do "import" acima)
        Scanner scanner = new Scanner(System.in);
        // declaracao das variaveis que receberao entradas
        int a;
        float b;
        boolean c;
        char d;
        String e;
        
        // leituras
        System.out.print("Digite um inteiro: ");
        a = scanner.nextInt();
        
        // Importante: em Java, o separador de casas decimais que o Scanner
        // compreende epende do *locale* padrão do seu computador, que eh 
        // influenciado  pelo idioma do seu sistema 
        // operacional, por exemplo (i.e. ponto, se 'en', e virgula, se 'pt-br')
        System.out.print("Digite um nro real: ");
        b = scanner.nextFloat();
        
        System.out.print("Digite um valor booleano: ");
        c = scanner.nextBoolean();
        
        System.out.print("Digite uma string: ");
        e = scanner.next();
        
        // le uma string, mas soh extrai o primeiro caractere
        System.out.print("Digite um valor caractere: ");
        d = scanner.next().charAt(0);
        
        System.out.printf("Inteiro: %d\n", a);
        System.out.printf("Real (float): %f\n", b);
        System.out.printf("Booleano: %s\n", c);
        System.out.printf("String: %s\n", e);
        System.out.printf("Caracter: %s\n", d);
        
    }
}
